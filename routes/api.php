<?php

use App\Http\Controllers\SedimentsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('sediments', SedimentsController::class)->except(
    'create', 'edit'
);

Route::get('teste', function() {
    return 1234;
});