<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSedimentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sediments', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('common_name', 100);
            $table->string('type', 50);
            $table->string('category', 50);
            $table->string('treatment_technology', 50);
            $table->string('class', 50);
            $table->string('unit_of_measurement', 30);
            $table->decimal('weight', 10, 3);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sediments');
    }
}
