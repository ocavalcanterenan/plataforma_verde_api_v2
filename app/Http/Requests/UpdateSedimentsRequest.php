<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateSedimentsRequest extends FormRequest
{
    public function rules()
    {
        $categories = ['Reciclàvel', 'Não Reciclável'];

        return [
            'common_name'           =>  ['nullable', 'string', 'max:100'],
            'type'                  =>  ['nullable', 'string', 'max:50'],
            'category'              =>  ['nullable', 'string', Rule::in($categories)],
            'treatment_technology'  =>  ['nullable', 'string', 'max:50'],
            'class'                 =>  ['nullable', 'string', 'max:50'],
            'unit_of_measurement'   =>  ['nullable', 'string', 'max:30'],
            'weight'                =>  ['nullable', 'numeric'],
        ];
    }
}
