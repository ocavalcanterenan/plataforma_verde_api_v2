<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSedimentsRequest extends FormRequest
{
    public function rules()
    {
        return [
            'file'   =>  ['required', 'file', 'mimes:xlsx, csv']
        ];
    }
}
