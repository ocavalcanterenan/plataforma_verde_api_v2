<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSedimentsRequest;
use App\Http\Requests\UpdateSedimentsRequest;
use App\Imports\SedimentsImport;
use App\Models\Sediment;

class SedimentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sediments = Sediment::simplePaginate();
        
        return response()->json($sediments, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSedimentsRequest $request)
    {
        $file = $request->file('file');

        $importFile = new SedimentsImport();
        $importFile->import($file);

        return response()->json('Arquivo colocado na fila de importação', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sediment = Sediment::findOrFail($id);

        return response()->json($sediment, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSedimentsRequest $request, $id)
    {
        $sediment = Sediment::findOrFail($id);
        $sediment->common_name = $request->input('common_name') ?? $sediment->common_name;
        $sediment->type = $request->input('type') ?? $sediment->type;
        $sediment->category = $request->input('category') ?? $sediment->category;
        $sediment->treatment_technology = $request->input('treatment_technology') ?? $sediment->treatment_technology;
        $sediment->class = $request->input('class') ?? $sediment->class;
        $sediment->unit_of_measurement = $request->input('unit_of_measurement') ?? $sediment->unit_of_measurement;
        $sediment->weight = $request->input('weight') ?? $sediment->weight;

        $sediment->save();

        return response()->json($sediment, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sediment = Sediment::findOrFail($id);

        $sediment->delete();

        return response()->json($sediment, 204);
    }
}
