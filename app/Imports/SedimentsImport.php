<?php

namespace App\Imports;

use App\Models\Sediment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class SedimentsImport implements ToModel, SkipsEmptyRows, WithHeadingRow, WithValidation, ShouldQueue, WithChunkReading
{
    use Importable, SkipsErrors, Queueable;

    public function model(array $row)
    {
        return new Sediment([
            'common_name'           =>  $row['nome_comum_do_residuo'],
            'type'                  =>  $row['tipo_de_residuo'],
            'category'              =>  $row['categoria'],
            'treatment_technology'  =>  $row['tecnologia_de_tratamento'],
            'class'                 =>  $row['classe'],
            'unit_of_measurement'   =>  $row['unidade_de_medida'],
            'weight'                =>  $row['peso'],
        ]);
    }

    public function rules(): array
    {
        $categories = ['Reciclàvel', 'Não Reciclável'];

        return [
            '*.nome_comum_do_residuo'   =>  ['required', 'string', 'max:100'],
            '*.tipo_de_residuo'         =>  ['required', 'string', 'max:50'],
            '*.categoria'               =>  ['required', 'string', Rule::in($categories)],
            '*.tecnologia_de_tratamento'=>  ['required', 'string', 'max:50'],
            '*.classe'                  =>  ['required', 'string', 'max:50'],
            '*.unidade_de_medida'       =>  ['required', 'string', 'max:30'],
            '*.peso'                    =>  ['required', 'numeric']
        ];
    }

    public function headingRow(): int
    {
        return 5;
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
