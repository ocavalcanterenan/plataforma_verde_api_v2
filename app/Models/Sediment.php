<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sediment extends Model
{
    use HasFactory;

    protected $fillable = [
        'common_name',
        'type',
        'category',
        'treatment_technology',
        'class',
        'unit_of_measurement',
        'weight',
    ];
}
