<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## ⚙ Configuração via Composer

* Link para a documentação da API:
> https://documenter.getpostman.com/view/12799523/UyxjFm2V

1- Para instalar dependências do projeto:
> composer install --ignore-platform-reqs

2- Depois de configurado o .env, build o ambiente docker:
> docker-compose up -d --build

3- Crie as tabelas via migration:
> php artisan migrate

4- Para startar os workers que vão realizar os jobs:
> php artisan queue:work

5- Para rodar os testes unitários e de integração:
> ./vendor/bin/phpunit    (Dentro do ambiente docker)