<?php

namespace Tests\Unit\Sediment;

use App\Models\Sediment;
use PHPUnit\Framework\TestCase;

class SedimentTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_check_if_sediments_columns_is_correct()
    {
        $sediment = new Sediment();

        $expected = [
            'common_name',
            'type',
            'category',
            'treatment_technology',
            'class',
            'unit_of_measurement',
            'weight',
        ];

        $arrayCompared = array_diff($expected, $sediment->getFillable());

        $this->assertEquals(0, count($arrayCompared));
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_check_if_sediments_columns_is_not_correct()
    {
        $sediment = new Sediment();

        $expected = [
            'test',
            'common_name',
            'type',
            'category',
            'treatment_technology',
            'class',
            'weight',
        ];

        $arrayCompared = array_diff($expected, $sediment->getFillable());

        $this->assertNotEquals(0, count($arrayCompared));
    }
}
