<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SedimentTest extends TestCase
{
    // use RefreshDatabase;
    public function test_list_sediments()
    {
        $response = $this->get('http://localhost/api/sediments/');
        
        $response->assertStatus(200);
    }

    public function test_find_sediments()
    {
        $response = $this->get('http://localhost/api/sediments/7');
        
        $response->assertStatus(200);
    }

    public function test_exception_find_nonexistent_sediment()
    {
        $response = $this->get('http://localhost/api/sediments/700');
        
        $response->assertStatus(404);
    }

    public function test_delete_sediment()
    {
        $response = $this->get('http://localhost/api/sediments/7');
        
        $response->assertStatus(200);
    }

    public function test_exception_delete_nonexistent_sediment()
    {
        $response = $this->get('http://localhost/api/sediments/700');
        
        $response->assertStatus(404);
    }

    public function test_update_nonexistent_sediment()
    {
        $response = $this->get('http://localhost/api/sediments/700');
        
        $response->assertStatus(404);
    }
}
